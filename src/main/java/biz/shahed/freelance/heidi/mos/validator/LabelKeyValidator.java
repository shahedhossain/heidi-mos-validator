package biz.shahed.freelance.heidi.mos.validator;

import java.io.Serializable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LabelKeyValidator implements ConstraintValidator<LabelKey, String>, Serializable {

	private static final long serialVersionUID = 8410115248258723213L;
	private static final String FORM_LBL_EX = "^[\\w]{3,30}$";
	private static final String RPRT_LBL_EX = "^[L]{1}[B]{1}[L]{1}[_]{1}[A-Z0-9]{2,30}$";
	
	LabelKey labelKey;

	@Override
	public void initialize(LabelKey labelKey) {
		this.labelKey = labelKey;
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext ctx) {
		if (value != null) {
			String pttrn = labelKey.isForm() ? FORM_LBL_EX : RPRT_LBL_EX;
			return value.matches(pttrn);			
		}		
		return false;
	}

}
