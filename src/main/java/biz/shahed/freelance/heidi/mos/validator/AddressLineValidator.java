package biz.shahed.freelance.heidi.mos.validator;

import java.io.Serializable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AddressLineValidator implements
		ConstraintValidator<AddressLine, String>, Serializable {

	private static final long serialVersionUID = 8410115248258723213L;

	private static final String REGEX_EN_OP = "^[\\w,.#:\\-/ ]{0,100}$";
	private static final String REGEX_EN_RE = "^[\\w,.#:\\-/ ]{1,100}$";
	private static final String REGEX_BN_OP = "^[\\p{InBengali}\\d\u0964,.#:\\-/ ]{0,200}$";
	private static final String REGEX_BN_RE = "^[\\p{InBengali}\\d\u0964,.#:\\-/ ]{1,200}$";

	private AddressLine address;

	@Override
	public void initialize(AddressLine address) {
		this.address = address;
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext ctx) {
		if (address.required()) {
			if (value != null) {
				if (address.lang() == Language.EN) {
					return value.matches(REGEX_EN_RE);
				} else if (address.lang() == Language.NA) {
					return value.matches(REGEX_BN_RE);
				}
			} else {
				return false;
			}
		} else {
			if (value != null) {
				if (address.lang() == Language.EN) {
					return value.matches(REGEX_EN_OP);
				} else if (address.lang() == Language.NA) {
					return value.matches(REGEX_BN_OP);
				}
			} else {
				return true;
			}
		}
		return false;
	}

}
