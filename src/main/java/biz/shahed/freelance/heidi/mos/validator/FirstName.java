package biz.shahed.freelance.heidi.mos.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = FirstNameValidator.class)
@Target(value = { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface FirstName {

	int lang() default Language.EN;
	
	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	String message() default "Enter valid first name";
}
