package biz.shahed.freelance.heidi.mos.validator;

import java.io.Serializable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class BloodGroupValidator implements ConstraintValidator<BloodGroup, String>, Serializable {

	private static final long serialVersionUID = 8410115248258723213L;

	private static final String REGEX_EN = "^[A|a|B|b|AB|ab|O|o]{1,2}[+-]{1}$";
	private static final String REGEX_BN = "^[\u098F|\u09AC\u09BF|\u098F\u09AC\u09BF|\u0993]{1,3}[+-]{1}$";

	private BloodGroup blood;

	@Override
	public void initialize(BloodGroup blood) {
		this.blood = blood;
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext ctx) {
		if (value != null && blood.lang() == Language.EN) {
			return value.matches(REGEX_EN);
		} else if (blood.lang() == Language.NA) {
			if (value != null) {
				return value.matches(REGEX_BN);
			}
			return true;
		}		
		return false;
	}

}
