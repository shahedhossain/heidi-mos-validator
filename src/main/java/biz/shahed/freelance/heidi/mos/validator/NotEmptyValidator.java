package biz.shahed.freelance.heidi.mos.validator;

import java.io.Serializable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NotEmptyValidator implements
		ConstraintValidator<NotEmpty, String>, Serializable {

	private static final long serialVersionUID = 2043822543330837484L;

	@Override
	public void initialize(NotEmpty notEmpty) {

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext ctx) {
		if (value != null) {
			String string = value.trim();
			return !string.isEmpty();
		}
		return false;
	}

}
