package biz.shahed.freelance.heidi.mos.validator;

import java.io.Serializable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * CURD For Create Update Read Delete<code><br/>
 * D U C R  CURD<br/>
 * ==============<br/>
 * 0 0 0 1 = 1  [R]<br/>
 * 0 0 1 0 = 2  [C]<br/>
 * 0 0 1 1 = 3  [CR]<br/>
 * 0 1 0 1 = 5  [UR]<br/>
 * 0 1 1 1 = 7  [CUR]<br/>
 * 1 0 0 1 = 9  [RD]<br/>
 * 1 0 1 1 = 11 [CRD]<br/>
 * 1 1 0 1 = 13 [URD]<br/>
 * 1 1 1 1 = 15 [CURD]</code>
 */
public class PermissionValidator implements ConstraintValidator<Permission, Integer>, Serializable {

	private static final long serialVersionUID = 8410115248258723213L;
	private final int[] VALUES = new int[] { 1, 2, 3, 5, 7, 9, 11, 13, 15 };

	@Override
	public void initialize(Permission permission) {

	}

	@Override
	public boolean isValid(Integer value, ConstraintValidatorContext ctx) {

		if (value != null) {
			int val = value.intValue();
			for (int ref : VALUES) {
				if (val == ref) {
					return true;
				}
			}
		}

		return false;
	}

}
