package biz.shahed.freelance.heidi.mos.validator;

import java.io.Serializable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PostCodeValidator implements ConstraintValidator<PostCode, Integer>, Serializable {

	private static final long serialVersionUID = 8410115248258723213L;
	
	private static final String POST_CODE = "^[\\d]{0,4}$";

	@Override
	public void initialize(PostCode postCode) {

	}

	@Override
	public boolean isValid(Integer value, ConstraintValidatorContext ctx) {
		if (value != null) {
			String postcode = String.format("%s", value);			
			return postcode.matches(POST_CODE);	
		}		
		return false;
	}

}
