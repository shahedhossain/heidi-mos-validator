package biz.shahed.freelance.heidi.mos.validator;

import java.io.Serializable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;



public class SubreportValidator implements ConstraintValidator<Subreport, String>, Serializable {

	private static final long serialVersionUID = -6770753670970668444L;
	private static final String REGEX = "R[0-9]{2}[B|I|T][0-9]{6}[X][0-9]{2}";
	
	@Override
	public void initialize(Subreport subreport) {
		
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext ctx) {
		if (value != null) {
			return value.matches(REGEX);
		}
		return false;
	}

}
