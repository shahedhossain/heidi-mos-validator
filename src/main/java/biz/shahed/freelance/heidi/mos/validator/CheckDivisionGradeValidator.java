package biz.shahed.freelance.heidi.mos.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.mvel2.MVEL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckDivisionGradeValidator implements ConstraintValidator<CheckDivisionGrade, Object> {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(CheckDivisionGradeValidator.class);

	private String fieldFlag;
	private String[] fieldSet1;
	private String[] fieldSet2;

	@Override
	public void initialize(CheckDivisionGrade checks) {
		this.fieldFlag = checks.fieldFlag();
		this.fieldSet1 = checks.fieldSet1();
		this.fieldSet2 = checks.fieldSet2();
	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		Object flagObject = MVEL.getProperty(fieldFlag, value);
		boolean flag = flagObject != null ? (Boolean) flagObject : false;

		if (flag) {
			for (String property : fieldSet1) {
				Object field = MVEL.getProperty(property, value);
				if (field == null) {
					return false;
				}
			}

			for (String property : fieldSet2) {
				MVEL.setProperty(value, property, null);
			}

		} else {
			for (String property : fieldSet2) {
				Object field = MVEL.getProperty(property, value);
				if (field == null) {
					return false;
				}
			}

			for (String property : fieldSet1) {
				MVEL.setProperty(value, property, null);
			}
		}

		return true;
	}

}
