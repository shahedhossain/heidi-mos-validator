package biz.shahed.freelance.heidi.mos.validator;

import java.io.Serializable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

//import org.jboss.seam.annotations.Transactional;

public class FormValidator implements ConstraintValidator<Form, String>, Serializable {

	private static final long serialVersionUID = 8410115248258723213L;
	private static final String REGEX = "F[0-9]{2}[B|I|R|T][0-9]{6}";

	@Override
	public void initialize(Form form) {

	}

	@Override
//	@Transactional TODO
	public boolean isValid(String value, ConstraintValidatorContext ctx) {
		if (value != null) {
			return value.matches(REGEX);
		}
		return false;
	}

}
