package biz.shahed.freelance.heidi.mos.validator;

import java.io.Serializable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;



public class LengthValidator implements ConstraintValidator<Length, String>, Serializable {

	private static final long serialVersionUID = 3024367633028796114L;
	private Length length;
	
	@Override
	public void initialize(Length length) {
		
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext ctx) {
		if (value != null) {
			int size = value.length();
			return size >= length.min() && size <= length.max();
		}
		return false;
	}

}
