package biz.shahed.freelance.heidi.mos.validator;

import java.io.Serializable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NoteValidator implements ConstraintValidator<Note, String>, Serializable {

	private static final long serialVersionUID = 8410115248258723213L;

	private static final String REGEX_EN_OP = "^[\\w,.#:\\-/ ]{0,45}$";
	private static final String REGEX_EN_RE = "^[\\w,.#:\\-/ ]{1,45}$";
	private static final String REGEX_BN_OP = "^[\\p{InBengali}\u0964,.#:\\-/ ]{0,100}$";
	private static final String REGEX_BN_RE = "^[\\p{InBengali}\u0964,.#:\\-/ ]{1,100}$";

	private Note note;

	@Override
	public void initialize(Note note) {
		this.note = note;
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext ctx) {
		if (note.required()) {
			if (value != null) {
				if (note.lang() == Language.EN) {
					return value.matches(REGEX_EN_RE);
				} else if (note.lang() == Language.NA) {
					return value.matches(REGEX_BN_RE);
				}
			} else {
				return false;
			}
		} else {
			if (value != null) {
				if (note.lang() == Language.EN) {
					return value.matches(REGEX_EN_OP);
				} else if (note.lang() == Language.NA) {
					return value.matches(REGEX_BN_OP);
				}
			} else {
				return true;
			}
		}
		return false;
	}

}
