package biz.shahed.freelance.heidi.mos.validator;

import java.util.Date;

import org.mvel2.MVEL;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UpdatedValidator implements
		ConstraintValidator<Updated, Object> {

	private String date;
	private String verifyDate;

	public void initialize(Updated updated) {
		this.date = updated.date();
		this.verifyDate = updated.verifyDate();
	}

	public boolean isValid(Object value, ConstraintValidatorContext context) {
		Object object = MVEL.getProperty(date, value);
		Object verifyObject = MVEL.getProperty(verifyDate, value);

		if (object == null && verifyObject != null) {
			return false;
		} else if (object != null && verifyObject == null) {
			return true;
		} else if (object != null && verifyObject != null) {
			long time = ((Date) object).getTime();
			long verifyTime = ((Date) verifyObject).getTime();
			return time > verifyTime;
		}
		
		return false;
	}
}
