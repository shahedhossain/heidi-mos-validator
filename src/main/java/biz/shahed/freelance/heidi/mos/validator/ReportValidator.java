package biz.shahed.freelance.heidi.mos.validator;

import java.io.Serializable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;



public class ReportValidator implements ConstraintValidator<Report, String>, Serializable {

	private static final long serialVersionUID = 8410115248258723213L;
	private static final String REGEX = "R[0-9]{2}[B|I|T][0-9]{6}";

	@Override
	public void initialize(Report report) {

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext ctx) {
		if (value != null) {
			return value.matches(REGEX);
		}
		return false;
	}

}
