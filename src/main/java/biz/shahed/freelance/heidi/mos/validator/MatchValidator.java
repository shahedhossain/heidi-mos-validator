package biz.shahed.freelance.heidi.mos.validator;

import org.mvel2.MVEL;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MatchValidator implements ConstraintValidator<Match, Object> {

	private String field;
	private String verifyField;

	public void initialize(Match matches) {
		this.field = matches.field();
		this.verifyField = matches.verifyField();
	}

	public boolean isValid(Object value, ConstraintValidatorContext context) {
		Object object = MVEL.getProperty(field, value);
		Object verifyObject = MVEL.getProperty(verifyField, value);

		if (object == null && verifyObject != null) {
			return false;
		} else if (object != null && verifyObject == null) {
			return false;
		} else if (object != null && verifyObject != null) {
			return object.equals(verifyObject);
		}

		return true;
	}
}
