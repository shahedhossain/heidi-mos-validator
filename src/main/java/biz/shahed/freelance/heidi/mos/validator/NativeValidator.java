package biz.shahed.freelance.heidi.mos.validator;

import java.io.Serializable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NativeValidator implements ConstraintValidator<Native, String>, Serializable {

	private static final long serialVersionUID = 8410115248258723213L;

	private static final String REGEX_BN_OP = "^[\\p{InBengali}\u0964,.#:\\-/ ]{0,100}$";
	private static final String REGEX_BN_RE = "^[\\p{InBengali}\u0964,.#:\\-/ ]{1,100}$";

	private Native bangla;

	@Override
	public void initialize(Native bangla) {
		this.bangla = bangla;
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext ctx) {
		if (bangla.required() && value != null) {
			return value.matches(REGEX_BN_RE);
		} else if(!bangla.required()){
			if (value != null) {
				return value.matches(REGEX_BN_OP);
			}
			return true;
		}
		return false;
	}

}
