package biz.shahed.freelance.heidi.mos.validator;

public interface Language {
	int EN = 0;
	int NA = 1;
}
