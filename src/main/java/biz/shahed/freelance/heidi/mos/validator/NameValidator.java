package biz.shahed.freelance.heidi.mos.validator;

import java.io.Serializable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NameValidator implements ConstraintValidator<Name, String>, Serializable {

	private static final long serialVersionUID = 8410115248258723213L;

	private static final String REGEX_EN_OP = "^[a-zA-Z.\\- ]{0,45}$";
	private static final String REGEX_EN_RE = "^[a-zA-Z.\\- ]{2,45}$";
	private static final String REGEX_BN_OP = "^[\\p{InBengali}.\\- ]{0,100}$";
	private static final String REGEX_BN_RE = "^[\\p{InBengali}.\\- ]{1,100}$";

	private Name name;

	@Override
	public void initialize(Name name) {
		this.name = name;
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext ctx) {
		if (name.required()) {
			if (value != null) {
				if (name.lang() == Language.EN) {
					return value.matches(REGEX_EN_RE);
				} else if (name.lang() == Language.NA) {
					return value.matches(REGEX_BN_RE);
				}
			} else {
				return false;
			}
		} else {
			if (value != null) {
				if (name.lang() == Language.EN) {
					return value.matches(REGEX_EN_OP);
				} else if (name.lang() == Language.NA) {
					return value.matches(REGEX_BN_OP);
				}
			} else {
				return true;
			}
		}
		return false;
	}

}
