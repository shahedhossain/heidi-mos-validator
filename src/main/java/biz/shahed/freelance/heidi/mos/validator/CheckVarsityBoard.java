package biz.shahed.freelance.heidi.mos.validator;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Retention(RUNTIME)
@Target( { TYPE, ANNOTATION_TYPE })
@Constraint(validatedBy = CheckVarsityBoardValidator.class)
public @interface CheckVarsityBoard {
	String message() default "Varsity/Board entry required";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	String fieldFlag();

	String[] fieldSet1() default{};
	
	String[] fieldSet2() default{};
}
