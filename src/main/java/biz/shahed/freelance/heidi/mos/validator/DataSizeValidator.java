package biz.shahed.freelance.heidi.mos.validator;

import java.io.Serializable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DataSizeValidator implements  ConstraintValidator<DataSize, byte[]>, Serializable {

	private static final long serialVersionUID = 8410115248258723213L;

	private DataSize dataSize;

	@Override
	public void initialize(DataSize dataSize) {
		this.dataSize = dataSize;
	}

	@Override
	public boolean isValid(byte[] value, ConstraintValidatorContext ctx) {
		if (value != null) {
			int size = value.length;
			return size >= dataSize.min() && size <= dataSize.max();
		}
		return true;
	}

}
