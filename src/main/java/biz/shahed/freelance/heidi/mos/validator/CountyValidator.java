package biz.shahed.freelance.heidi.mos.validator;

import java.io.Serializable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CountyValidator implements  ConstraintValidator<County, String>, Serializable {

	private static final long serialVersionUID = 8410115248258723213L;

	private static final String REGEX_EN = "^[a-zA-Z.\\- ]{2,45}$";
	private static final String REGEX_BN = "^[\\p{InBengali}.\\- ]{0,100}$";

	private County county;

	@Override
	public void initialize(County county) {
		this.county = county;
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext ctx) {
		if (value != null && county.lang() == Language.EN) {
			return value.matches(REGEX_EN);
		} else if (county.lang() == Language.NA) {
			if (value != null) {
				return value.matches(REGEX_BN);
			}
			return true;
		}		
		return false;
	}

}
