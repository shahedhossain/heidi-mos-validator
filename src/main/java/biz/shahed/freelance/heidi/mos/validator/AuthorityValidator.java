package biz.shahed.freelance.heidi.mos.validator;

import java.io.Serializable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AuthorityValidator implements ConstraintValidator<Authority, String>, Serializable {

	private static final long serialVersionUID = 8410115248258723213L;

	private static final String REGEX_RE = "^ROLE_C[\\d]{2}[B|I|J|Q|R|S|T|X]{1}[\\d]{6}_(([S|B]{1}[C|R|U|D]{1})|JR)[\\d]{3}$";

	private Authority authority;

	@Override
	public void initialize(Authority authority) {
		this.authority = authority;
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext ctx) {
		if (authority.required() && value != null) {
			return value.matches(REGEX_RE);
		} else if(!authority.required()){
			return true;
		}
		return false;
	}

}
