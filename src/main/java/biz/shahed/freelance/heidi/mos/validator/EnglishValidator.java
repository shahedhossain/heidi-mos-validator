package biz.shahed.freelance.heidi.mos.validator;

import java.io.Serializable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EnglishValidator implements ConstraintValidator<English, String>, Serializable {

	private static final long serialVersionUID = 8410115248258723213L;

	private static final String REGEX_OP = "^[\\w,.#:\\-/ ]{0,45}$";
	private static final String REGEX_RE = "^[\\w,.#:\\-/ ]{1,45}$";

	private English english;

	@Override
	public void initialize(English english) {
		this.english = english;
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext ctx) {
		if (english.required() && value != null) {
			return value.matches(REGEX_RE);
		} else if(!english.required()){
			if (value != null) {
				return value.matches(REGEX_OP);
			}
			return true;
		}
		return false;
	}

}
