package biz.shahed.freelance.heidi.mos.validator;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Retention(RUNTIME)
@Target( { TYPE, ANNOTATION_TYPE })
@Constraint(validatedBy = BackdatedValidator.class)
public @interface Backdated {

	String message() default "Verified date field is updated";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	String date();

	String verifyDate();
}
