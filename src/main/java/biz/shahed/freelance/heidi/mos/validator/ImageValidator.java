package biz.shahed.freelance.heidi.mos.validator;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import javax.imageio.ImageIO;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ImageValidator implements ConstraintValidator<Image, byte[]>, Serializable {

	private static final long serialVersionUID = 2981476819729091373L;

	private Image image;

	@Override
	public void initialize(Image image) {
		this.image = image;
	}

	@Override
	public boolean isValid(byte[] value, ConstraintValidatorContext ctx) {
		boolean retFlag = false;
		if (value != null) {
			try {
				InputStream bis = new ByteArrayInputStream(value, 0,value.length);
				BufferedImage bi = ImageIO.read(bis);
				int w = bi.getWidth();
				int h = bi.getHeight();
				return w == image.width() && h == image.height();
			} catch (IOException e) {
				retFlag = false;
			}
		} else {
			retFlag = true;
		}
		return retFlag;
	}
}
