package com.formativesoft.mcserp.validator;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class ValidatorTest extends TestCase {

	public ValidatorTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(ValidatorTest.class);
	}

	public void testFirst() {
		assertTrue(true);
	}
	
	public void testSecond() {
		assertTrue(true);
	}
	
	public void testMore() {
		String regex = "^[\\w,.#:\\-/ ]{0,100}$";
		assertTrue("shahed123".matches(regex));
		regex = "^[a-zA-Z,.#:\\-/ ]{0,100}$";
		assertFalse("shahed123".matches(regex));
		assertTrue(true);
		regex = "^[\\p{InBengali}\\d\u0964,.#:\\-/ ]{0,200}$";
		assertTrue("\u09a2\u09be\u0995\u09be #123".matches(regex));
		regex = "^[\\p{InBengali}\u0964,.#:\\-/ ]{0,200}$";
		assertFalse("\u09a2\u09be\u0995\u09be #123".matches(regex));
	}
}
